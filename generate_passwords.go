//  +build ignore

//
//  Created by Elliott Polk on 06/05/2016
//  Copyright © 2016 Cathay Pacific Airways Ltd. All rights reserved.
//
package main

import (
	"flag"
	"fmt"

	"bitbucket.org/innovationcentre/passwords"
)

const OutMin int = 3

var (
	length      *int  = flag.Int("len", passwords.CharMin, "character count per password")
	count       *int  = flag.Int("count", OutMin, "total passwords to generate")
	incSpecials *bool = flag.Bool("specials", false, "include special characters (e.g. !@#$%^&*)")

	encrypt *bool   = flag.Bool("encrypt", false, "encrypt the resulting passwords")
	cert    *string = flag.String("cert", "", "path to cert for encryption")
)

func main() {
	flag.Parse()

	if *encrypt && len(*cert) < 1 {
		panic(fmt.Errorf("must provide a valid cert for encryption"))
	}

	for i := 0; i < *count; i++ {
		pwd, err := passwords.New(*length, *incSpecials)
		if err != nil {
			panic(err)
		}
		fmt.Printf("password %d:    %s\n", i, pwd)

		if *encrypt {
			ciphertxt, err := passwords.Encrypt(pwd, *cert)
			if err != nil {
				panic(err)
			}
			fmt.Printf("encrypted:     %s\n\n", string(ciphertxt))
		}
	}
}
