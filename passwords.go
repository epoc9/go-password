//
//  Created by Elliott Polk on 06/05/2016
//  Copyright © 2016 Cathay Pacific Airways Ltd. All rights reserved.
//
package passwords

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"fmt"
	"io"
	"io/ioutil"
)

var AlphaNum = []byte("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789")
var Specials = []byte(`~!@#$%^&*()-+=[]{}:;,.?\|/"'<>`)

const CharMin int = 12

//  New attempts to generate a pseudo-random password of the provided length. If
//  the flag incSpecials is true, the pseudo-random password will include values
//  from Specials as well as alphanumeric.
func New(length int, incSpecials bool) (string, error) {
	if length < CharMin {
		length = CharMin
	}

	res := make([]byte, length)
	buf := make([]byte, length+(length/4))

	//  Create a new array with the characters to "pick from" and append the
	//  AlphaNum values. If the incSpecials value is true, append the Specials
	//  as well.
	chars := make([]byte, len(AlphaNum))
	copy(chars, AlphaNum)
	if incSpecials {
		chars = append(chars, Specials...)
	}

	clen := len(chars)
	maxbuf := 512 - (512 % clen)

	i := 0
	for { //  run until we get the correct length
		if _, err := io.ReadFull(rand.Reader, buf); err != nil {
			return "", err
		}

		for _, dat := range buf {
			c := int(dat)
			if c > maxbuf {
				continue //  skip to avoid modulo bias
			}

			res[i] = chars[c%clen]
			i++

			if i == length {
				return string(res), nil
			}
		}
	}
}

func Encrypt(password, certPath string) ([]byte, error) {
	certBytes, err := ioutil.ReadFile(certPath)
	if err != nil {
		return nil, err
	}

	//  ignoring the "rest" byte array since it is not used
	block, _ := pem.Decode(certBytes)

	cert, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		return nil, err
	}

	pubKey, ok := cert.PublicKey.(*rsa.PublicKey)
	if !ok {
		return nil, fmt.Errorf("invalid cert provided")
	}

	//  encrypt with public cert
	ciphertext, err := rsa.EncryptPKCS1v15(rand.Reader, pubKey, []byte(password))
	if err != nil {
		return nil, err
	}

	//  encode to base64 and return
	return []byte(base64.StdEncoding.EncodeToString(ciphertext)), nil
}
