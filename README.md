## Usage:
```
#!bash

# help
$ go run generate_passwords.go -h

# generate and list pseudo-random passwords
$ go run generate_passwords.go

# to encrypt for use in config files
$ go run generate_passwords.go -encrypt -cert=<path_to_cert_file>.pem
```

## To produce a runnable binary and not rely on *go*:
```
#!bash

$ go build -o passwords generate_passwords.go
```